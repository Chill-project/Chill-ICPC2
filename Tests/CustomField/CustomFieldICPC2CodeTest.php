<?php

/*
 *  Chill is a software for social workers
 * Copyright (C) 2015, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\ICPC2Bundle\Tests\CustomField;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\CustomFieldsBundle\Entity\CustomField;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CustomFieldICPC2CodeTest extends KernelTestCase
{
        
    /**
     *
     * @var \Chill\CustomFieldsBundle\Service\CustomFieldProvider
     */
    private $cfProvider;
    
    /**
     *
     * @var \Chill\ICPC2Bundle\CustomField\CustomFieldICPC2Code
     */
    private $cfICPC2;
    
    public function setUp()
    {
        static::bootKernel();
        
        $this->cfProvider = static::$kernel->getContainer()
                ->get('chill.custom_field.provider');
        $this->cfICPC2 = $this->cfProvider->getCustomFieldByType('ICPC2');
    }
    
    /**
     * 
     * @param array $options
     * @return CustomField
     */
    private function generateCustomField($options = array())
    {
        return (new CustomField())
            ->setActive(true)
            ->setSlug('slug')
            ->setOptions(array())
            ->setType('ICPC2')
                ;
    }
    
    /////////////////////////////////////////
    //
    // test function isEmptyValue 
    //
    ////////////////////////////////////////
    
    
    public function testIsEmtpyWithEmptyValue()
    {
        $cf = $this->generateCustomField();
        
        $data = $this->cfICPC2->deserialize(null, $cf);;
        
        $isEmpty = $this->cfICPC2->isEmptyValue($data, $cf);
        
        $this->assertTrue($isEmpty);
    }
    
    public function testIsEmtpyWithNonEmptyValue()
    {
        $cf = $this->generateCustomField();
        
        $data = $this->cfICPC2->deserialize('A05', $cf);
        
        $isEmpty = $this->cfICPC2->isEmptyValue($data, $cf);
        
        $this->assertFalse($isEmpty);
        
        $cf = $this->generateCustomField();
    }
    
    
    /////////////////////////////////////////
    //
    // test function deserialize 
    //
    ////////////////////////////////////////
    
    public function testDeserialized()
    {
        $cf = $this->generateCustomField();
        
        $code1 = $this->cfICPC2->deserialize('A05', $cf);
        $code2 = $this->cfICPC2->deserialize('D01', $cf);
        $codeNull = $this->cfICPC2->deserialize(null, $cf);
        
        $this->assertInstanceOf('Chill\ICPC2Bundle\Entity\Code', $code1);
        $this->assertInstanceOf('Chill\ICPC2Bundle\Entity\Code', $code2);
        $this->assertNull($codeNull);
    }
    
    /////////////////////////////////////////
    //
    // test function serialize 
    //
    ////////////////////////////////////////
    
    public function testSerialize()
    {
        $repository = self::$kernel->getContainer()->get('doctrine.orm.entity_manager')
                ->getRepository('ChillICPC2Bundle:Code');
        $codeA07 = $repository->findOneBy(array('code' => 'A07'));
        $code32  = $repository->findOneBy(array('code' => -32));
        $cf = $this->generateCustomField();
        
        $serializedA07  = $this->cfICPC2->serialize($codeA07, $cf);
        $serialized32   = $this->cfICPC2->serialize($code32, $cf);
        $serializedNull = $this->cfICPC2->serialize(null, $cf);
        
        $this->assertEquals('A07', $serializedA07);
        $this->assertEquals(-32, $serialized32);
        $this->assertNull($serializedNull);
    }
    
    
}
